# TP - Calculatrice

## Intégration

Recopier la maquette le plus fidèlement possible.

## Programmation

Faire fonctionner la calculatrice, selon le cahier des charges suivant :

##### Boutons :

- [ ] `[0-9]` affiche le nombre dans l'écran 
- [ ] `+`, `-`, `/`, `x`, `. affiche le symbole dans l'écran 
- [ ] `C` efface le dernier caractère écrit dans l'écran 
- [ ] `R` efface l'intégralité de l'écran, et réinitialise le calcul en cours 
- [ ] `=` effectue le calcul affiché dans l'écran, puis y affiche le résultat _(en écrasant le contenu actuel)_

#### Gestions des erreurs :

- [ ] Quand une erreur survient afficher `ERR` dans l'écran
- [ ] Empêcher l'utilisateur d'écrire un calcul menant à une erreur (ignorer le clic en "sortant" immédiatement de la fonction)
    - [ ] mettre plusieurs `.` au sein d'un même nombre (ex `2 + 3.2.4`)
    - [ ] mettre plusieurs symboles illogiques d'affilés (ex `2 +/ 3` est interdit, mais `2 * -3` est autorisé)
    - [ ] *diviser un nombre par zéro*
    - *il en manque sans doute, à compléter selon vos observations ...*
    
## Conseils :

- Utiliser le cahier des charges pour valider votre avancée pas à pas (**faire au moins un commit pour chaque étape du cahier des charges**)
- Créer autant de branches que vous le souhaitez
- Utiliser la fonction `eval` de Javascript pour effectuer le calcul.
- Ajouter autant de classes CSS que nécessaire
- Utiliser les fonctions `querySelector` et / ou `querySelectorAll` sélectionne les éléments.
- Garder en tête que `querySelectorAll` retourne une liste. **On ne peut pas cliquer sur une liste !**, mais on peut cliquer sur chaque élément de cette liste.
- Eventuellement, utiliser le fait que le navigateur associe automatiquement un identifiant html à une variable globale. (**ne pas abuser de cette technique, préférer `querySelector`**)

    ```html
    <button id="myButton">Je suis un bouton</button>
    ```
  
    ```js
      // la variable "myButton" est créée automatiquement par le navigateur, et fait référence au noeud du DOM associé
     console.log(myButton.textContent); // "Je suis un bouton"
     ```
